/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.Principal;


/*
10.Fazer um programa para ler uma quantidade N de alunos.
Ler a nota de cada um dos N alunos e calcular a média aritmética das notas.
Contar quantos alunos estão com a nota acima de 7.0.
Obs.: Se nenhum aluno tirou nota acima de 5.0, imprimir mensagem:
Não há nenhum aluno com nota acima de 5.
*/



public class Principal {
    String nome;
    int nota01;
    int nota02;
    int nota03;
    
    
    public Principal (){
        
    }

    public Principal(String nome, int nota01, int nota02, int nota03, int media) {
        this.nome = nome;
        this.nota01 = nota01;
        this.nota02 = nota02;
        this.nota03 = nota03;
        
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNota01() {
        return nota01;
    }

    public void setNota01(int nota01) {
        this.nota01 = nota01;
    }

    public int getNota02() {
        return nota02;
    }

    public void setNota02(int nota02) {
        this.nota02 = nota02;
    }

    public int getNota03() {
        return nota03;
    }

    public void setNota03(int nota03) {
        this.nota03 = nota03;
    }



    
    public double calcular (double media){
        media = (nota01 + nota02 + nota03) / 3;
        
    return media;}
    
    
    
}
